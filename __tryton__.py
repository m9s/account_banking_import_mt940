# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Banking Import MT940',
    'name_de_DE': 'Buchhaltung Bankimport MT940',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Bank Import Method MT940
    - Provides the import method for Message Type 940

    MT940 is a format often used by Online Banking Programs as an interface to
    other accounting programs.
''',
    'description_de_DE': '''Bankimportmethode MT940
    - Stellt die Importmethode für den Nachrichtentyp MT940 bereit

    MT940 ist ein durch Online-Banking-Programme als Schnittstelle zu anderen
    Buchhaltungsprogrammen häufig gebrauchtes Format.
''',
    'depends': [
        'account_banking_import',
    ],
    'xml': [
        'banking_import.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
