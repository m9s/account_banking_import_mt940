Banking Import MT940 Module
###########################

This module provides the import method for Message Type 940

MT940 is a format often used by Online Banking Programs as an interface to
other accounting programs.


Known working exports
=====================

Exports in the following lists are known to work with this parser:


Programs
--------
Starmoney Business
SFirm32 (Sparkasse)



Export of webinterfaces
-----------------------

Sparkasse Freiburg, Aachen


