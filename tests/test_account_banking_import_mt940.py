#!/usr/bin/env python
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import sys, os
DIR = os.path.abspath(os.path.normpath(os.path.join(__file__,
    '..', '..', '..', '..', '..', 'trytond')))
if os.path.isdir(DIR):
    sys.path.insert(0, os.path.dirname(DIR))

import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import DB_NAME, USER, CONTEXT
from trytond.tests.test_tryton import test_view, test_depends
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.modules.account_banking_import import BankingImportMethod


class AccountBankingImportMt940TestCase(unittest.TestCase):
    '''
    Test AccountBankingImportMt940 module.
    '''

    def setUp(self):
        trytond.tests.test_tryton.install_module('account_banking_import_mt940')
        self.fpath = os.path.dirname(__file__)

    def test0005views(self):
        '''
        Test views.
        '''
        test_view('account_banking_import_mt940')

    def test0006depends(self):
        '''
        Test depends.
        '''
        test_depends()

    def test0010invalid_end_balance(self):
        fn = 'test0010_invalid_end_balance.txt'
        data = open(os.path.join(self.fpath, fn), 'r').read()
        with Transaction().start(DB_NAME, USER, context=CONTEXT):
            self.assertRaises(UserError, BankingImportMethod('mt940').parse, 1,
                data)

    def test0020invalid_code(self):
        fn = 'test0020_invalid_code.txt'
        data = open(os.path.join(self.fpath, fn), 'r').read()
        with Transaction().start(DB_NAME, USER, context=CONTEXT):
            self.assertRaises(UserError, BankingImportMethod('mt940').parse, 1,
                data)

    def test0030invalid_date(self):
        fn = 'test0030_invalid_date.txt'
        data = open(os.path.join(self.fpath, fn), 'r').read()
        with Transaction().start(DB_NAME, USER, context=CONTEXT):
            self.assertRaises(UserError, BankingImportMethod('mt940').parse, 1,
                data)

def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        AccountBankingImportMt940TestCase))
    return suite

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(suite())
