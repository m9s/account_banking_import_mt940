# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from decimal import Decimal
import re
from trytond.model import ModelView, ModelSQL, fields
from trytond.error import WarningErrorMixin
from trytond.modules.account_banking_import import BankingImportMethod
from trytond.modules.account_banking_import.utils import str2date

_IMPORT_METHOD = ('mt940', 'MT 940')

class BankingImportConfiguration(ModelSQL, ModelView):
    _name = 'banking.import.configuration'

    def __init__(self):
        super(BankingImportConfiguration, self).__init__()
        self.import_method = copy.copy(self.import_method)
        if _IMPORT_METHOD not in self.import_method.selection:
            self.import_method.selection.append(_IMPORT_METHOD)
        self._reset_columns()

BankingImportConfiguration()


class BankingImportLine(ModelSQL, ModelView):
    _name = 'banking.import.line'

    code = fields.Char('Code', readonly=True)
    posting_text = fields.Char('Posting Text', readonly=True)
    primanota = fields.Char('Primanota', readonly=True)
    names = fields.Char('Names', readonly=True)

BankingImportLine()


class ImportMethodMT940(BankingImportMethod, WarningErrorMixin):
    'Import Method MT 940'
    _name = 'mt940'

    def __init__(self, name):
        super(ImportMethodMT940, self).__init__()
        self._error_messages = {
            'invalid_end_balance': 'Invalid MT940 file!\n\nThe computed end '
                'balance does not match the reported end balance.\n'
                'Reported end balance: %s\nComputed end balance: %s',
            'invalid_code': 'Invalid MT940 file!\n\nThe value %s is an invalid '
                'transaction code.',
            'invalid_date': 'Invalid MT940 file!\n\nCannot convert %s to date.',
            }

    def parse(self, import_config, data):
        '''
        Parse data into Banking Import Lines.

        :param import_config: the id of the Import Configuration
        :param data: the data to be parsed

        :returns: a list containing
                - a dict with the data to update the record
                  of the Banking Import File with
                  key: field
                  value: value as string
                - a list of dicts with the data to be put
                  in Banking Import Lines with
                  key: field
                  value: value as string
        '''
        parse_result = {
                'log': 'Parsing of data failed with unknown reason!',
                'state': 'failed',
                }
        parsed_lines = []

        account_recipient = ''
        bank_code_recipient = ''
        date = '' # Date
        valuta_date = '' # Date
        iban = ''
        bic = ''
        account_payer = ''
        bank_code_payer = ''
        amount = Decimal('0.0') # Numeric
        currency = '' # ISO String -> to be converted later Many2One
        purpose = ''
        start_balance = Decimal('0.0') # Numeric
        end_balance = Decimal('0.0') # Numeric
        running_balance = Decimal('0.0') # Numeric

        lines = data.splitlines()

        batch = 1
        rlog = ''
        multiline = False

        # Loop over 61 and 86 field before appending
        imp_line = {}
        for line in lines:
            # imp_line['control'] = line
            # skip empty lines, order reference, statement reference
            if len(line) == 0 or line[:4] == ':20:' or line[:5] == ':28C:':
                continue
           # bank code + bank account recipient
            if line[:4] == ':25:':
                bank_code_recipient = line[4:12]
                account_recipient = line[13:].lstrip('0')
                account_recipient = re.sub('[A-Z]{1,3}$', '', account_recipient)
                continue
            # total start balance, date of balance, currency ISO4217
            elif line[:5] == ':60F:':
                sign = line[5]
                sdate = line[6:12]
                currency = line[12:15]
                sbal = line[15:]

                if sign == 'D':
                    sbal = '-' + sbal
                start_balance = Decimal(sbal.replace(',','.'))

                running_balance = start_balance
                continue
            # amount, valuta date and date, start + end balance
            elif line[:4] == ':61:':
                if multiline == True:
                    multiline = False
                    parsed_lines.append(imp_line)
                    imp_line = {}
                vdate = line[4:10]
                bdate = line[10:14]
                sign = line[14]
                amnt = line[16:]
                amnt = amnt.split('N')[0]

                if sign == 'D':
                    amnt = '-' + amnt
                amnt = Decimal(amnt.replace(',','.'))

                # bdate has no year, must be taken from vdate
                year = vdate[:2]
                # handle year change
                if vdate[2:4] == '12' and bdate[:2] == '01':
                    year = str(int(year) + 1)
                elif vdate[2:4] == '01' and bdate[:2] == '12':
                    year = str(int(year) - 1)
                year = year.rjust(2, '0')
                bdate = year + bdate

                imp_line['start_balance'] = running_balance
                running_balance += amnt
                imp_line['end_balance'] = running_balance
                try:
                    imp_line['valuta_date'] = str2date(vdate, format='%y%m%d')
                except:
                    self.raise_user_error('invalid_date', error_args=(vdate,))

                try:
                    imp_line['date'] = str2date(bdate, format='%y%m%d')
                except:
                    self.raise_user_error('invalid_date', error_args=(bdate,))

                imp_line['amount'] = amnt

                # append global values
                imp_line['bank_import_config'] = import_config
                imp_line['account_recipient'] = account_recipient
                imp_line['bank_code_recipient'] = bank_code_recipient
                imp_line['currency'] = currency
                continue
            # purposes
            elif line[:4] == ':86:':
                imp_line['purpose'] = line[4:]
                multiline = True
                continue
            # total end balance, date of end balance, currency ISO4217
            elif line[:5] == ':62F:':
                if multiline == True:
                    multiline = False
                    parsed_lines.append(imp_line)
                    imp_line = {}
                sign = line[5]
                edate = line[6:12]
                currency = line[12:15]
                ebal = line[15:]

                if sign == 'D':
                    ebal = '-' + ebal
                end_balance = Decimal(ebal.replace(',','.'))

                rlog += '\nBatch No.: %s\n' % batch + \
                    'Transmitted End Balance: %s\n' % end_balance + \
                    'Calculated End Balance: %s\n' % running_balance

                if end_balance - running_balance == 0:
                    rlog += 'Correct balance check.\n'
                    parse_result['state'] = 'parsed'
                else:
                    self.raise_user_error('invalid_end_balance', error_args=(
                        end_balance, running_balance))
                batch += 1
            # append this line, previous field is multiline
            elif multiline == True:
                imp_line['purpose'] = imp_line['purpose'] + line

        # Parse in a second step field86 into its components
        final_lines = []
        for line in parsed_lines:
            final_lines.append(self._split_purpose(line))

        log = 'Parsing Results:\n---------------------\n\n' \
                'Transactions: %s\n' % len(final_lines) + rlog
        parse_result['log'] = log
        res = [parse_result, final_lines]
        return res

    def _split_purpose(self, line):
        '''
        Split the information of the purpose field in its relative parts
        '''
        purpose = line['purpose']

        vals = copy.copy(line)
        code = purpose[:3]
        try:
            code = int(code)
        except:
            self.raise_user_error('invalid_code', error_args=(code,))
        if code < 1 or code > 999:
            self.raise_user_error('invalid_code', error_args=(code,))
        vals['code'] = code

        # posting_text
        pat_str = '\?00.*\?10'
        pattern = re.compile(pat_str)
        res = pattern.findall(purpose)
        if res:
            vals['posting_text'] = res[0][3:-3]

        # primanota
        pat_str = '\?10.*\?20'
        pattern = re.compile(pat_str)
        res = pattern.findall(purpose)
        if res:
            vals['primanota'] = res[0][3:-3]

        # real_purpose
        real_purpose = ''
        for number in range(20, 30):
            pat_str = '\?' + str(number) + '.*\?' + str(number + 1) + \
                    '|\?' + str(number) + '.*\?30' + \
                    '|\?' + str(number) + '.*$'
            pattern = re.compile(pat_str)
            res = pattern.findall(purpose)
            if res:
                real_purpose += res[0][3:-3] + '\n'
        vals['purpose'] = real_purpose

        #bank_code_payer
        pat_str = '\?30.*\?31'
        pattern = re.compile(pat_str)
        res = pattern.findall(purpose)
        if res:
            vals['bank_code_payer'] = res[0][3:-3]

        # account_payer
        pat_str = '\?31.*\?32'
        pattern = re.compile(pat_str)
        res = pattern.findall(purpose)
        if res:
            vals['account_payer'] = res[0][3:-3]

        # names
        pat_str = '\?32.*\?34|\?32.*$'
        pattern = re.compile(pat_str)
        res = pattern.findall(purpose)
        if res:
            names = res[0][3:-3]
            names = pattern.findall(purpose)[0][3:]
            names = names.replace('?33', ' ').replace('?34', '')
            vals['names'] = names

        return vals
